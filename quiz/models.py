from django.db import models

class Quiz(models.Model):
    title = models.CharField(max_length=200)
    description = models.CharField(max_length=400)
    date = models.DateTimeField("date published")

class Factor(models.Model):
    title = models.CharField(max_length=200)
    description = models.CharField(max_length=400)
    quiz = models.ForeignKey(Quiz, on_delete=models.CASCADE)

class Question(models.Model):
    propostion = models.CharField(max_length=400)
    factor = models.ForeignKey(Factor, on_delete=models.CASCADE)
    quiz = models.ForeignKey(Quiz, on_delete=models.CASCADE)
